const fs = require('fs-extra');
const path = require('path');
const util = require('gulp-util');
const gulp = require('gulp');
const merge = require('merge-stream');
const csso = require('gulp-csso');
const uglify = require('gulp-uglify');
const base64 = require('gulp-css-base64');
const imagemin = require('gulp-imagemin');
const header = require('gulp-header');
const sass = require('gulp-sass');
const autoprefix = require('gulp-autoprefixer');
const uuid = require('uuid/v4');
const glob = require('glob');

const PRODUCTION = util.env.env === 'prod' ? true : false;
const settings = {
	imageQuality: PRODUCTION ? 7 : 0
};

const resourcesDir = path.join(__dirname, 'resources');
const buildDir = path.join(__dirname);
const pkg = require('./package.json');

const license = [
	"/*",
	"* This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. ",
	"* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.",
	`* Copyright (c) 2014 - ${(new Date()).getFullYear()} Caprine Softworks - for LazyPurple.net`,
	"*/",
	""
].join("\n");


gulp.task('license', () => {
	return gulp.src([
		buildDir + '/dist/js/*.js',
		buildDir + '/dist/css/*.css',
	], { base: './dist' })
	.pipe(header(license))
	.pipe(gulp.dest('./dist'));
});


gulp.task('fonts', function() {
	gulp.src([
		resourcesDir + '/font/*.{ttf,woff,woff2,eot,svg}',
	])
	.pipe(gulp.dest('./dist/font'));
});


gulp.task('sass', function() {
	gulp.src([
		resourcesDir + '/sass/app.scss',
	])
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefix({
		browsers: 'last 50 versions, last 30 iOS versions, last 30 Android versions, last 20 bb versions, last 20 and_chr versions, ie 6-9, > 1%'
	}))
	.pipe(csso())
	.pipe(gulp.dest('./dist/css'));
});


gulp.task('images', () => {
	gulp.src([
		'./resources/img/*.{jpeg,jpg,gif,png,bmp,svg}'
	])
	.pipe(imagemin([
		imagemin.gifsicle({ interlaced: true }),
		imagemin.jpegtran({ progressive: true }),
		imagemin.optipng({ optimizationLevel: settings.imageQuality }),
		imagemin.svgo({
			plugins: [
				{ removeViewBox: true },
				{ cleanupIDs: false }
			]
		})
	], { verbose: true }))
	.pipe(gulp.dest('./dist/img'));
});


gulp.task('cleanup', ['cleanupThumbsDB'], () => {
	glob(`${buildDir}/dist/**/*.{jpg,gif,png,svg,mp3,db,css,js,json,mp4,webm,wav,ogg,otf,ttf,eot,woff,woff2}`, (e, f) => {
		f.forEach(file => {
			fs.unlink(path.resolve('./', file), (err) => {
				if (err && err.code != 'ENOENT') throw new Error(err);
				return util.log(`Cleared built file:`, file);
			});
		});
	});
});


gulp.task('cleanupThumbsDB', () => {
	glob(`${resourcesDir}/**/*.db`, (e, f) => {
		f.forEach(file => {
			fs.unlink(path.join('./', file), (err) => {
				if (err && err.code != 'ENOENT') throw new Error(err);
				return util.log(`Deleted DB file:`, file);
			});
		});
	});

	glob(`./**/*.db`, (e, f) => {
		f.forEach(file => {
			fs.unlink(path.join('./', file), (err) => {
				if (err && err.code != 'ENOENT') throw new Error(err);
				return util.log(`Deleted DB file:`, file);
			});
		});
	});
});


gulp.task('default', ['fonts', 'sass', 'images']);