<?php
/**
 * This file is part of lazypurple.net.
 * lazypurple.net is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lazypurple.net is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lazypurple.net.  If not, see <https://www.gnu.org/licenses/>.
 */
define('ROOT', __DIR__);
define('INCLUDES_DIR', ROOT . '/includes');

require_once(INCLUDES_DIR . '/helpers.php');
$conf = require_once(INCLUDES_DIR . '/config.php');

?><!DOCTYPE html><?php /* This is just a harmless comment put at the very top of all pages. Please leave it in as it is to credit me without being obtrusive! */ require_once(INCLUDES_DIR . '/credit.php');?>
<html lang="en" dir="ltr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title><?php echo $conf->site_title;?></title>

		<meta name="theme-color" content="<?php echo $conf->site_color;?>">

		<meta name="url" content="<?php echo $conf->site_root;?>">
		<meta name="referrer" content="unsafe-url">
		<link rel="canonical" content="<?php echo $conf->site_root;?>">

		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="<?php echo $conf->site_title;?>">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<meta name="application-name" content="<?php echo $conf->site_title;?>">
		<meta name="msapplication-starturl" content="<?php echo $conf->site_root;?>">
		<meta name="msapplication-TileColor" content="<?php echo $conf->site_color;?>">
		<meta name="msapplication-navbutton-color" content="<?php echo $conf->site_color;?>">

		<meta property="og:site_name" content="<?php echo $conf->site_title;?>">
		<meta property="og:locale" content="en_US">

		<!--[if lt IE 9]><link rel="shortcut icon" type="image/x-icon" href="/">-->

		<link rel="stylesheet" href="<?php echo bust('/dist/css/app.css');?>">
	</head>
	<body>
		<noscript>
			<div class="notification is-danger">This website requires you to have JavaScript enabled.</div>
		</noscript>
		<main id="mount"></main>
		<script type="text/javascript" src="<?php echo bust('/dist/js/app.js');?>"></script>
	</body>
</html>