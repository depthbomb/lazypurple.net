<?php
/**
 * This file is part of lazypurple.net.
 * lazypurple.net is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lazypurple.net is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lazypurple.net.  If not, see <https://www.gnu.org/licenses/>.
 */

	function u(string $path = '') : string {
		return sprintf(
			'%s://%s%s%s',
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 8000 ? ":{$_SERVER['SERVER_PORT']}" : '',
			$path
		);
	}

	/**
	 * Generates a GUIDv4
	 *
	 * @param integer $salt				Optional numeric salt
	 * @param boolean $braces			Wrap output in curly braces
	 * @param boolean $uppercase		Returns the string in all uppercase
	 * @param boolean $classFriendly	Makes the GUID usable in HTML classes or IDs
	 */
	function guid(int $salt = null, bool $braces = false, bool $uppercase = false, bool $classFriendly = false) : string {
		srand(is_int($salt) ? $salt : (double) microtime() * 10000 + rand());
		$left_brace = chr(123);
		$right_brace = chr(125);
		$uuid = sprintf(
			'%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			rand(0, 0xffff),
			rand(0, 0xffff),
			// 16 bits for "time_mid"
			rand(0, 0xffff),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			rand(0, 0x0fff) | 0x4000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			rand(0, 0x3fff) | 0x8000,
			// 48 bits for "node"
			rand(0, 0xffff),
			rand(0, 0xffff),
			rand(0, 0xffff)
		);

		if ($uppercase) {
			$uuid = strtoupper($uuid);
		}
		
		if ($braces) {
			$uuid = $left_brace . $uuid . $right_brace;
		}

		srand();	//	Reset the seeding

		return $uuid;
	}

	/**
	* Bust
	*
	* Creates a formatted URL to the provided path and appends a cachebuster string to the end
	* @param string $url URL/Path to file
	* @return string
	*/
	function bust(string $path) : string {
		$seed = filemtime(ROOT . $path);
		$hash = guid($seed);
		return u("{$path}?v={$hash}");
	}