<?php
/**
 * This file is part of lazypurple.net.
 * lazypurple.net is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lazypurple.net is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lazypurple.net.  If not, see <https://www.gnu.org/licenses/>.
 */

return (object) [
	'site_title' => 'LazyPurple.net',
	'site_color' => '#4b06a1',
	'site_root' => u(),
];