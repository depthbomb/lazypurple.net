import m from 'mithril';

let appLoaded = false;
let onSplash = false;
const mount = document.getElementById('mount');

if (!appLoaded && !onSplash) {
	onSplash = true;
	window.location.href = '#!/';
}

preloadImages = (array) => {
	if (!preloadImages.list) {
		preloadImages.list = [];
	}
	var list = preloadImages.list;
	for (var i = 0; i < array.length; i++) {
		var img = new Image();
		img.onload = () => {
			var index = list.indexOf(this);
			if (index !== -1) {
				// remove image from the array once it's loaded
				// for memory consumption reasons
				list.splice(index, 1);
			}
		}
		list.push(img);
		img.src = array[i];
	}
}

icon = (name) => {
	return m('span', { class: `icon icon-${name}` }, '');
};

const Template = {
	view: (vnode) => {
		return m(".container", [
			m(".logo"),
			m(".section", vnode.children)
		]);
	}
};

const Splash = {
	oninit: () => {
		onSplash = true;
		preloadImages(['/dist/img/pattern-d-10.png', '/dist/img/logo.png']);
		setTimeout(() => {
			window.location.href = '#!/home';
			appLoaded = true;
		}, 1000);
	},
	view: () => {
		return m("h1", {
			class: 'title is-size-0 has-text-white-bis has-text-centered'
		}, [
			"Loading",
			m.trust("&hellip;")
		]);
	}
};

const Home = {
	view: () => {
		return m(Template, [
			m('.columns', [
				m('.column', m("a.button.is-primary.is-huge.is-slanted.is-fullwidth", {
						href: ''
					},
					m("span.slanted-content.has-text-weight-bold", [
						icon('discord'), " Discord"
					])
				)),
				m('.column', m("a.button.is-primary.is-huge.is-slanted.is-fullwidth", {
						href: 'http://steamcommunity.com/groups/lazypurple'
					},
					m("span.slanted-content.has-text-weight-bold", [
						icon('steam'), " Steam Group"
					])
				)),
				m('.column', m("a.button.is-primary.is-huge.is-slanted.is-fullwidth", {
						href: 'http://192.223.26.238/fastdl/'
					},
					m("span.slanted-content.has-text-weight-bold", [
						icon('download'), " Fast DL"
					])
				)),
			]),
			m('.columns', [
				m('.column', m("a.button.is-primary.is-fullwidth", {
						href: 'steam://connect/192.223.26.238:27015'
					},
					m("span", "Silly Server 1")
				)),
				m('.column', m("a.button.is-primary.is-fullwidth", {
						href: 'steam://connect/192.223.26.238:27016'
					},
					m("span", "Silly Server 2")
				)),
				m('.column', m("a.button.is-primary.is-fullwidth", {
						href: 'steam://connect/192.223.26.238:27017'
					},
					m("span", "Competitive Server")
				)),
				m('.column', m("a.button.is-primary.is-fullwidth", {
						href: 'steam://connect/192.223.26.238:27018'
					},
					m("span", "MvM Server")
				)),
			])
		]);
	}
};

m.route(mount, "/", {
	"/": Splash,
	"/home": Home,
});